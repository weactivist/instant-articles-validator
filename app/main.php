<?php
	require __DIR__ . '/vendor/autoload.php';

	use Facebook\InstantArticles\Validators\InstantArticleValidator;
	use Facebook\InstantArticles\Parser\Parser;

	$client = new MongoDB\Client("mongodb://mongo:27017");

	try {
		$client->listDatabases();
	}
	catch( MongoDB\Driver\Exception\ConnectionTimeoutException $e ) {
		exit($e->getMessage());
	}

	$collection = $client->admin->instantarticles;

	foreach( $collection->find(['status' => 'waiting']) as $document )
	{
		$instantArticle = Parser::parse($document->content);
		$validator = InstantArticleValidator::check($instantArticle);

		if(!is_array($validator)) {
			$validator = [];
		}

		if( count( $validator ) > 0 ) {
			$collection->updateOne(
				['_id' => $document->_id],
				['$set' => ['status' => 'error', 'error' => 'Instant Article markup is not valid.']]
			);
		}
		else {
			$collection->updateOne(
				['_id' => $document->_id],
				['$set' => ['status' => 'valid']]
			);
		}
	}
?>
